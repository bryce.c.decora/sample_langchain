from dotenv import load_dotenv
from PyPDF2 import PdfReader
from langchain.text_splitter import CharacterTextSplitter
from langchain.embeddings import OpenAIEmbeddings, HuggingFaceInstructEmbeddings
from langchain.chat_models import ChatOpenAI
from langchain.chains import RetrievalQA
from langchain import PromptTemplate
from langchain.document_loaders.git import GitLoader
from langchain.document_loaders import YoutubeLoader, DataFrameLoader
import pandas as pd
from langchain.vectorstores import Chroma
from langchain.tools import Tool
from langchain.agents import initialize_agent, AgentType, load_tools

sales_template = """#Overall
You're a super cool bot named @rep_name who is meant to be fun support for CloseBot.ai users.  @lead_name is already a paid CloseBot user with the @plan_label!  @lead_name may have questions about the product and it's your job to answer questions they may have, subtly driving them to buy a subscription!  @lead_name is in the following industry ```@industry```. Here is some information about the CloseBot company:

- Integrates via a snapshot with GoHighLevel CRMs like REIReply
- Can integrate with non-GoHighLevel CRMs via API (salesforce, hubspot, podio)
- There are 3 paid tiers 1. $39/month (gives you access to a single bot and up to 2,000 messages per month) 2. $99/month (access to up to 3 bots and up to 6,000 messages/month) 3. $199/month (unlimited bots and up to 10,000 messages per month)
- Above 10,000 messages per month a user must ask about enterprise pricing
- Cost per message is $0.01 for GPT-3.5, $0.03 for Davinci or $0.06 for GPT-4
- Done for you setup is available, cost depending on the CRM they use ($350 for GoHighLevel and $800 for Podio)

This conversation between you and @lead_name should be a lot of fun 💪 Use emoji's when you have a chance and be humorous when you can to keep @lead_name engaged.  If @lead_name wants to hire us to integrate the bot for them, we can absolutely do that!

#About the CloseBot website:

- lefthand side has the options Account Dashboard (where to see message history and usage), Modify Bot (where the bot settings are to edit the bot), Test Your Bot (where to go to do internal testing on your bot), Articles (where to learn more about best practices), API Documentation (where to find details about how to tie into CloseBot via API), Videos (link to instructional videos YouTube page)

Useful links you can share when necessary:
Helpful Articles: https://closebot.ai/recent-posts/
YouTube Channel: https://www.youtube.com/@closebot

If the context is not relevant, please answer the question by using your own knowledge about the topic.

{context}

User Response: {question}"""
sales_description = """useful for knowing the voice in which you need to respond to the user question"""


support_template = """
Your name is Tom with Neo4j support, If the context is not relevant, 
        please answer the question by using your own knowledge about the topic

{context}

User Response: {question}"""
support_description = """useful for when when a user asks to optimize or debug a Cypher statement or needs
                       specific instructions how to accomplish a specified task. 
                       Input should be a fully formed question."""


def git(url, path, branch, target, splitter):
    # GDS guides
    loader = GitLoader(
        clone_url=url,
        repo_path=path,
        branch=branch,
        file_filter=lambda file_path: file_path.endswith(".adoc")
        and target in file_path,
    )
    data = loader.load()
    split_data = splitter.split_documents(data)
    return split_data

def csv(url, splitter):
    article_url = url
    medium = pd.read_csv(article_url, sep=";")
    medium["source"] = medium["url"]
    medium_loader = DataFrameLoader(
        medium[["text", "source"]],
        page_content_column="text")
    medium_data = medium_loader.load()
    medium_data_split = splitter.split_documents(medium_data)
    return medium_data_split

def youtube(id, splitter):
    yt_loader = YoutubeLoader(id)
    yt_data = yt_loader.load()
    yt_data_split = splitter.split_documents(yt_data)
    return yt_data_split

def get_pdf_text(pdf_docs):
    text = ""
    for pdf in pdf_docs:
        pdf_reader = PdfReader(pdf)
        for page in pdf_reader.pages:
            text += page.extract_text()
    return text


def get_text_chunks(text):
    text_splitter = CharacterTextSplitter(
        separator="\n",
        chunk_size=1000,
        chunk_overlap=200,
        length_function=len
    )
    chunks = text_splitter.split_text(text)
    return chunks


def get_vectorstore(data, label):
    embeddings = OpenAIEmbeddings()
    store = Chroma.from_documents(
        data, embeddings, collection_name=label
    )
    return store

def get_qa(data, llm, template, label):
    store = get_vectorstore(
        data=data,
        label=label
    )
    PROMPT = PromptTemplate(
        template=template, input_variables=["context", "question"]
    )
    qa = RetrievalQA.from_chain_type(
        llm=llm,
        chain_type="stuff",
        retriever=store.as_retriever(),
        chain_type_kwargs={"prompt": PROMPT},
    )
    return qa

def get_tool(qa, description, label):
    tool = Tool(
        name=label,
        func=qa.run,
        description=description,
    )
    return tool

def main():
    load_dotenv()
    # Define text chunk strategy
    splitter = CharacterTextSplitter(
        chunk_size=2000,
        chunk_overlap=50,
        separator=" "
    )
    youtube_data = youtube(
        id="1sRgsEKlUr0",
        splitter=splitter
    )
    # gds_data = git(
    #     url="https://github.com/neo4j/graph-data-science",
    #     path="./repos/gds/",
    #     branch="master",
    #     target="pages",
    #     splitter=splitter
    # )
    kb_data = git(
        url="https://github.com/neo4j-documentation/knowledge-base",
        path="./repos/kb/",
        branch="master",
        target="articles",
        splitter=splitter
    )
#    csv_data = csv(   SSL error when doing this
#        url="https://raw.githubusercontent.com/tomasonjo/blog-datasets/main/medium/neo4j_articles.csv",
#        splitter=splitter
#    )
    llm = ChatOpenAI(
        model_name="gpt-3.5-turbo",
    )
    tools = load_tools(["pal-math", "serpapi"], llm=llm)
    sales_qa = get_qa(data=youtube_data, llm=llm, template=sales_template, label="sales")
    tools.append(get_tool(qa=sales_qa, description=sales_description, label="sales"))
    support_qa = get_qa(data=kb_data, llm=llm, template=support_template, label="support")
    tools.append(get_tool(qa=support_qa, description=support_description, label="support"))

    agent = initialize_agent(
        tools,
        llm,
        agent=AgentType.ZERO_SHOT_REACT_DESCRIPTION,
        verbose=True,
        handle_parsing_errors=True,
    )
    prompt = input("Message: ")
    agent.run(prompt)
    print("done")

if __name__ == '__main__':
    main()
