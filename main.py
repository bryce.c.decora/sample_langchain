import os
import sys
from dotenv import load_dotenv
from langchain.document_loaders import PyPDFLoader
from langchain.document_loaders import Docx2txtLoader
from langchain.document_loaders import TextLoader
from langchain.embeddings import OpenAIEmbeddings
from langchain.vectorstores import Chroma
from langchain.chat_models import ChatOpenAI
from langchain.chains import ConversationalRetrievalChain
from langchain.text_splitter import CharacterTextSplitter
from langchain.prompts import SystemMessagePromptTemplate, ChatPromptTemplate, HumanMessagePromptTemplate, PromptTemplate

load_dotenv('.env')

documents = []
# Create a List of Documents from all of our files in the ./docs folder
for file in os.listdir("docs"):
    if file.endswith(".pdf"):
        pdf_path = "./docs/" + file
        loader = PyPDFLoader(pdf_path)
        documents.extend(loader.load())
    elif file.endswith('.docx') or file.endswith('.doc'):
        doc_path = "./docs/" + file
        loader = Docx2txtLoader(doc_path)
        documents.extend(loader.load())
    elif file.endswith('.txt'):
        text_path = "./docs/" + file
        loader = TextLoader(text_path)
        documents.extend(loader.load())

# Split the documents into smaller chunks
text_splitter = CharacterTextSplitter(chunk_size=1000, chunk_overlap=10)
documents = text_splitter.split_documents(documents)

# Convert the document chunks to embedding and save them to the vector store
vectordb = Chroma.from_documents(documents, embedding=OpenAIEmbeddings(), persist_directory="./data")
vectordb.persist()

# create our Q&A chain
general_system_template = r""" 
Keep your response under 5 tokens.  Your main goal is to determine the user's interest in selling real estate.  Ask a question to probe to see whether they would be interested in selling property.
Use the following pieces of context to answer the users question.
 ----
{context}
----
{chat_history}
"""
general_user_template = "{question}"
messages = [
            SystemMessagePromptTemplate.from_template(general_system_template),
            HumanMessagePromptTemplate.from_template(general_user_template)
]
qa_prompt = ChatPromptTemplate.from_messages( messages )

custom_template = """Repeat the following:  {question}"""  # Tried this, doesn't work
CONDENSE_QUESTION_PROMPT_CUSTOM = PromptTemplate.from_template(custom_template)

pdf_qa = ConversationalRetrievalChain.from_llm(
    ChatOpenAI(temperature=.5, model_name='gpt-4'),
    retriever=vectordb.as_retriever(),
    return_source_documents=True,
    verbose=True,
    combine_docs_chain_kwargs={'prompt': qa_prompt},
    condense_question_prompt=CONDENSE_QUESTION_PROMPT_CUSTOM  #  Need to modify this part that auto-formats whatever the user says as a question
)

yellow = "\033[0;33m"
green = "\033[0;32m"
white = "\033[0;39m"

chat_history = []
print(f"{yellow}---------------------------------------------------------------------------------")
print('Welcome to the DocBot. You are now ready to start interacting with your documents')
print('---------------------------------------------------------------------------------')
while True:
    query = input(f"{green}Prompt: ")
    if query == "exit" or query == "quit" or query == "q" or query == "f":
        print('Exiting')
        sys.exit()
    if query == '':
        continue
    result = pdf_qa(
        {
            "question": query,
            "chat_history": chat_history
        })
    print(f"{white}Answer: " + result["answer"])
    chat_history.append((query, result["answer"]))